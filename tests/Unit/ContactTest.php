<?php

namespace Tests\Unit;

use App\Models\Contact;
use App\Models\Group;
use App\Models\User;
use Illuminate\Database\QueryException;
use Tests\TestCase;

class ContactTest extends TestCase {

    public function test_users_logged_can_create_contacts()
    {
        $user = $this->signIn();

        factory(Contact::class)->create(['user_id' => null]);

        $this->assertEquals(1, $user->contacts()->count());
    }

    public function test_contact_must_belongs_to_a_user()
    {
        $this->expectException(QueryException::class);

        factory(Contact::class)->create(['user_id' => null]);
    }

    public function test_the_contact_can_associate_a_group()
    {
        $group = factory(Group::class)->create();
        $contact = factory(Contact::class)->create();

        $contact->group()->associate($group);

        $this->assertEquals($contact->group->id, $group->id);
    }

    public function test_user_can_assign_contacts()
    {
        $user = $this->signIn();

        factory(Contact::class, 3)->create();

        $this->assertEquals($user->contacts()->count(), 3);
    }

    public function test_user_can_remove_contacts()
    {
        $user = $this->signIn();

        factory(Contact::class, 3)->create();

        $user->contacts()->first()->delete();

        $this->assertEquals(2, $user->contacts()->count());
    }

    public function test_contact_has_id_of_the_logged_user_after_update()
    {
        $userLogged = $this->signIn();
        $userNotLogged = factory(User::class)->create();
        $contact = factory(Contact::class)->create();

        $contact->update(['name' => 'new-name', 'user_id' => $userNotLogged->id]);

        $this->assertEquals($contact->user_id, $userLogged->id);
        $this->assertEquals('new-name', $contact->name);
    }

    public function test_only_the_owner_user_of_contact_can_find_the_contact()
    {
        //I have a contact of a dummy user
        $contact = factory(Contact::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to find the contact.
        $this->assertNull($contact->find($contact->id));

        //Now I have a contact of a logged user
        $contact = factory(Contact::class)->create();

        //I should be able to find the contact.
        $this->assertEquals($contact->id, $contact->find($contact->id)->id);
    }

    public function test_only_the_owner_user_of_contact_can_update_the_contact()
    {
        //I have a contact of a dummy user
        $contact = factory(Contact::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to update the contact.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertFalse(!! $contact->where('id', $contact->id)->update(['name' => 'new-name']));

        //Now I have a contact of a logged user
        $contact = factory(Contact::class)->create();

        //I should be able to update the contact.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertTrue(!! $contact->where('id', $contact->id)->update(['name' => 'new-name']));
        $this->assertEquals('new-name', $contact->find($contact->id)->name);
    }

    public function test_only_the_owner_user_of_contact_can_delete_the_contact()
    {
        //I have a contact of a dummy user
        $contact = factory(Contact::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to delete the contact
        $this->assertFalse(!! $contact->destroy($contact->id));

        //Now I have a contact of a logged user
        $contact = factory(Contact::class)->create();

        //I should be able to delete the contact
        $this->assertTrue(!! $contact->destroy($contact->id));
    }

}
