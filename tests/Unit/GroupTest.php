<?php

namespace Tests\Unit;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\QueryException;
use Tests\TestCase;

class GroupTest extends TestCase {

    public function test_users_logged_can_create_groups()
    {
        $user = $this->signIn();

        factory(Group::class)->create(['user_id' => null]);

        $this->assertEquals(1, $user->groups()->count());
    }

    public function test_group_must_belongs_to_a_user()
    {
        $this->expectException(QueryException::class);

        factory(Group::class)->create(['user_id' => null]);
    }

    public function test_user_can_assign_groups()
    {
        $user = $this->signIn();

        factory(Group::class, 3)->create();

        $this->assertEquals($user->groups()->count(), 3);
    }

    public function test_user_can_remove_groups()
    {
        $user = $this->signIn();

        factory(Group::class, 3)->create();

        $user->groups()->first()->delete();

        $this->assertEquals(2, $user->groups()->count());
    }

    public function test_group_has_id_of_the_logged_user_after_update()
    {
        $userLogged = $this->signIn();
        $userNotLogged = factory(User::class)->create();
        $group = factory(Group::class)->create();

        $group->update(['title' => 'new-title', 'user_id' => $userNotLogged->id]);

        $this->assertEquals($group->user_id, $userLogged->id);
        $this->assertEquals('new-title', $group->title);
    }

    public function test_only_the_owner_user_of_group_can_find_the_group()
    {
        //I have a group of a dummy user
        $group = factory(Group::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to find the group.
        $this->assertNull($group->find($group->id));

        //Now I have a group of a logged user
        $group = factory(Group::class)->create();

        //I should be able to find the group.
        $this->assertEquals($group->id, $group->find($group->id)->id);
    }

    public function test_only_the_owner_user_of_group_can_update_the_group()
    {
        //I have a group of a dummy user
        $group = factory(Group::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to update the group.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertFalse(!! $group->where('id', $group->id)->update(['title' => 'new-title']));

        //Now I have a group of a logged user
        $group = factory(Group::class)->create();

        //I should be able to update the group.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertTrue(!! $group->where('id', $group->id)->update(['title' => 'new-title']));
        $this->assertEquals('new-title', $group->find($group->id)->title);
    }

    public function test_only_the_owner_user_of_group_can_delete_the_group()
    {
        //I have a group of a dummy user
        $group = factory(Group::class)->create();

        //I make a login with another dummy user
        $this->signIn();

        //I should not be able to delete the group
        $this->assertFalse(!! $group->destroy($group->id));

        //Now I have a group of a logged user
        $group = factory(Group::class)->create();

        //I should be able to delete the group
        $this->assertTrue(!! $group->destroy($group->id));
    }

}
