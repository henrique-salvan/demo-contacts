<?php

namespace Tests\Unit;

use App\Models\User;
use Hash;
use Tests\TestCase;

class UserTest extends TestCase {

    public function test_password_is_saved_encrypted()
    {
        $user = factory(User::class)->create([
            'password' => 'my strong password here',
        ]);

        $this->assertTrue(Hash::check('my strong password here', $user->password), 'The password is not saved encrypted');
    }

    public function test_password_is_updated_encrypted()
    {
        $user = factory(User::class)->create();

        $user->update(['password' => 'my strong password here']);

        $this->assertTrue(Hash::check('my strong password here', $user->password), 'The password is not updated encrypted');
    }

    public function test_email_cant_be_duplicated()
    {
        $userOne = factory(User::class)->create([
            'email' => 'myemail@only.test',
        ]);

        $this->expectException(\PDOException::class);

        $userTwo = factory(User::class)->create([
            'email' => 'myemail@only.test',
        ]);
    }

    public function test_a_user_can_find_only_himself()
    {
        $userNotLogged = factory(User::class)->create();
        $userLogged = $this->signIn();

        $this->assertNull($userNotLogged->find($userNotLogged->id));
        $this->assertEquals($userLogged->id, $userLogged->find($userLogged->id)->id);
    }

    public function test_a_user_can_update_only_himself()
    {
        $userNotLogged = factory(User::class)->create();
        $userLogged = $this->signIn();

        //I should not be able to update the other user.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertFalse(!! $userNotLogged->where('id', $userNotLogged->id)->update(['name' => 'new-name']));

        //I should be able to update myself.
        //Using the Builder to update because the Model doesn't use the global scopes.
        $this->assertTrue(!! $userLogged->where('id', $userLogged->id)->update(['name' => 'new-name']));
        $this->assertEquals('new-name', $userLogged->find($userLogged->id)->name);
    }

}
