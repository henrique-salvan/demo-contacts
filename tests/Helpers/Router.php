<?php

namespace Tests\Helpers;

trait Router {

    private static $prefix = '/api/';

    private function insertToken($parameters)
    {
        if ($this->user) {
            $parameters['demo-contacts-token'] = $this->user->api_token;
        }
        return $parameters;
    }

    public function route($route = [], array $parameters = [])
    {
        return self::$prefix . join('/', is_array($route) ? $route : [$route]) . '?' . http_build_query(
            $this->insertToken($parameters)
        );
    }

}
