<?php

namespace Tests\Feature;

use App\Models\User;

class LoginTest extends Feature
{
    public function test_users_can_do_a_login()
    {
        $credentials = ['password' => 'my-pass-wd', 'email' => 'my-email@test.com'];
        $user = factory(User::class)->create($credentials);

        $this->post($this->route('login'), $credentials)
             ->assertJsonFragment(['name' => $user->name, 'email' => $user->email, 'api_token' => $user->api_token]);
    }

    public function test_user_can_be_retrieved_by_token()
    {
        $user = $this->signIn();

        $this->get($this->route(['login', 'user']))
             ->assertJsonFragment(['name' => $user->name, 'email' => $user->email, 'api_token' => $user->api_token]);
    }
}
