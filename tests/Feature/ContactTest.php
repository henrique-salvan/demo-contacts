<?php

namespace Tests\Feature;

use App\Models\Contact;

class ContactTest extends Feature
{
    protected function setUp()
    {
        parent::setUp();
        $this->signIn();
    }

    public function test_contacts_can_be_listed()
    {
        $contactOne = factory(Contact::class)->create();
        $contactTwo = factory(Contact::class)->create();

        $this->get($this->route('contacts'))
             ->assertJsonFragment(['name' => $contactOne->name])
             ->assertJsonFragment(['name' => $contactTwo->name]);
    }

    public function test_contact_can_be_created()
    {
        $contact = factory(Contact::class)->make();

        $this->post($this->route('contacts'), $contact->toArray())
             ->assertJsonFragment(['name' => $contact->name, 'group_id' => $contact->group_id]);
    }

    public function test_contact_can_be_updated()
    {
        $contact = factory(Contact::class)->create();

        $this->put($this->route(['contacts', $contact->id]), ['name' => 'new-name'])
             ->assertJsonFragment(['name' => 'new-name']);
    }

    public function test_contact_can_be_retrieved()
    {
        $contact = factory(Contact::class)->create();

        $this->get($this->route(['contacts', $contact->id]))
            ->assertJsonFragment(['name' => $contact->name, 'id' => $contact->id]);
    }

    public function test_contact_can_be_deleted()
    {
        $contact = factory(Contact::class)->create();

        $this->delete($this->route(['contacts', $contact->id]))
             ->assertJsonFragment(['success' => true]);
    }
}
