<?php

namespace Tests\Feature;

use App\Models\User;

class UserTest extends Feature
{
    public function test_users_can_be_created_without_authentication()
    {
        $user = factory(User::class)->raw(['api_token' => null]);

        $this->post($this->route('users'), $user)
             ->assertJsonFragment(['name' => $user['name'], 'email' => $user['email']]);
    }

    public function test_user_can_be_updated()
    {
        $user = $this->signIn();

        $this->put($this->route(['users', $user->id]), ['name' => 'new-name', 'email' => 'my-new-email@test.com'])
             ->assertJsonFragment(['name' => 'new-name', 'email' => 'my-new-email@test.com']);
    }

    public function test_user_can_be_retrieved()
    {
        $user = $this->signIn();

        $this->get($this->route(['users', $user->id]))
            ->assertJsonFragment(['name' => $user->name, 'id' => $user->id]);
    }
}
