<?php

namespace Tests\Feature;

use App\Models\Group;

class GroupTest extends Feature
{

    protected function setUp()
    {
        parent::setUp();
        $this->signIn();
    }

    public function test_groups_can_be_listed()
    {
        $groupOne = factory(Group::class)->create();
        $groupTwo = factory(Group::class)->create();

        $this->get($this->route('groups'))
             ->assertJsonFragment(['title' => $groupOne->title])
             ->assertJsonFragment(['title' => $groupTwo->title]);
    }

    public function test_group_can_be_created()
    {
        $group = factory(Group::class)->make();

        $this->post($this->route('groups'), $group->toArray())
             ->assertJsonFragment(['title' => $group->title]);
    }

    public function test_group_can_be_updated()
    {
        $group = factory(Group::class)->create();

        $this->put($this->route(['groups', $group->id]), ['title' => 'new-title'])
             ->assertJsonFragment(['title' => 'new-title']);
    }

    public function test_group_can_be_retrieved()
    {
        $group = factory(Group::class)->create();

        $this->get($this->route(['groups', $group->id]))
            ->assertJsonFragment(['title' => $group->title, 'id' => $group->id]);
    }

    public function test_group_can_be_deleted()
    {
        $group = factory(Group::class)->create();

        $this->delete($this->route(['groups', $group->id]))
             ->assertJsonFragment(['success' => true]);
    }
}
