<?php

namespace App\Http\Traits;

use App\Http\Filters\FilterCollection;

trait HttpHelpers {

    private static $defaultPerPage = 50;
    private static $perPageLimit = 100;

    public function transformResponse($data, $success = true)
    {
        $return = [];

        if (is_bool($data)) {
            $return['success'] = $data;
        } else {
            if (is_object($data)) {
                if (method_exists($data, 'toArray')) {
                    $return['content'] = $data->toArray();
                } else {
                    $return['success'] = false;
                    $return['content'] = "The object must have a \"toArray\" implementation";
                }
            } else {
                $return['content'] = $data;
            }
            $return['success'] = $success;
        }

        return $return;
    }

    public function getErrorsMessages($errors) {
        $response = ['errors' => []];
        if (isset($errors)) foreach ($errors as $field => $messages) {
            foreach ($messages as $message) $response['errors'][] = [
                'message' => $message
            ];
        }
        return $response;
    }

    public function getWiths()
    {
        $with = request('withs');
        if (strlen($with)) {
            return explode(',', $with);
        }
        return [];
    }

    public function getColumns()
    {
        $columns = request('columns');
        if (strlen($columns)) {
            return explode(',', $columns);
        }
        return [];
    }

    public function getPerPage()
    {
        $perPage = (int) request('per-page');

        if ($perPage >= 1) {
            if ($perPage <= self::$perPageLimit) {
                return $perPage;
            } else {
                return self::$perPageLimit;
            }
        }

        return self::$defaultPerPage;
    }

    public function getPage()
    {
        $page = (int) request('page');

        return ($page >= 1) ? $page : 1;
    }

    public function getFilterCollection()
    {
        return new class extends FilterCollection {

            public function __construct()
            {
                parent::__construct(request('filters'));
            }

        };
    }

}
