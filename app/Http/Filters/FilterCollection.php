<?php

namespace App\Http\Filters;

use App\Http\Filters\Conditions\Factory;

abstract class FilterCollection
{

    /**
     * Array of instances of conditions
     *
     * @var FilterInterface[] $conditionsInstances
     */
    private $conditionsInstances = [];

    /**
     * FiltersCollection constructor.
     *
     *
     * Array with filters pre defined, pass with this pattern:
     * 1 - column, 2 - condition (needed class defined), 3 - filter
     * [
     *      ['content', 'like', '%a%'],
     *      ['type', 'in', ['protected', 'private']],
     *      ['type', 'notIn', ['private']],
     *      ['type', 'equals', 'public'],
     * ]
     * @param $arrayConditionsFactories
     * @throws \Exception
     */
    public function __construct($arrayConditionsFactories)
    {
        $this->populateConditionsInstances($arrayConditionsFactories);
    }

    /**
     * Cycles through each filter and make instances
     *
     * @param array $arrayConditionsFactories
     * @throws \Exception
     */
    private function populateConditionsInstances($arrayConditionsFactories)
    {
        if (is_array($arrayConditionsFactories) && count($arrayConditionsFactories)) {
            foreach ($arrayConditionsFactories as $factory) {
                if ($factoryInstance = Factory::make(...$factory)) {
                    array_push($this->conditionsInstances, $factoryInstance);
                }
            }
        }
    }

    /**
     * Apply all filter on the query
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function applies($query)
    {
        if ($this->conditionsInstances && count($this->conditionsInstances)) {
            foreach ($this->conditionsInstances as $conditionInstance) {
                $conditionInstance->apply($query);
            }
        }

        return $query;
    }

}
