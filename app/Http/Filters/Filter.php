<?php

namespace App\Http\Filters;

abstract class Filter implements FilterInterface
{
    protected $column;

    protected $filter;

    public function setColumn($column)
    {
        $this->column = $column;
    }

    public function setFilter($filter)
    {
        $this->filter = $filter;
    }
}
