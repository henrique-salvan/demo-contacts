<?php

namespace App\Http\Filters\Conditions;

use App\Http\Filters\Filter;

class Equals extends Filter
{

    /**
     * @var string
     */
    private $operator = '=';

    /**
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     */
    public function apply(&$query)
    {
        $query->where($this->column, $this->operator, $this->filter);
    }

}
