<?php

namespace App\Http\Filters\Conditions;

use App\Http\Filters\Filter;

class Like extends Filter
{
    /**
     * Prevent for some bad values
     *
     * @var array
     */
    private $notAllowed = ['%%'];

    /**
     * @var string
     */
    private $operator = 'like';

    /**
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     */
    public function apply(&$query)
    {
        if (!in_array($this->filter, $this->notAllowed)) {
            $query->where($this->column, $this->operator, $this->filter);
        }
    }

}
