<?php

namespace App\Http\Filters\Conditions;

use App\Http\Filters\Filter;

class In extends Filter
{

    /**
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder $query
     */
    public function apply(&$query)
    {
        $query->whereIn($this->column, $this->filter);
    }

}
