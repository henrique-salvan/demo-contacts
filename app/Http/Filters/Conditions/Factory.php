<?php

namespace App\Http\Filters\Conditions;

use App\Http\Filters\FilterInterface;

class Factory
{

    /**
     * @var array $allowedConditions
     */
    private static $allowedConditions = [
        'Like', 'In', 'Equals'
    ];

    /**
     * @param $condition
     * @return FilterInterface
     */
    private static function getInstanceCondition($condition)
    {
        return app(__NAMESPACE__ . '\\' . $condition);
    }

    /**
     * Create a new filter instance
     *
     * @param string $column
     * @param string $condition
     * @param mixed $filter
     * @throws \Exception
     * @return FilterInterface|boolean
     */
    public static function make($column, $condition, $filter)
    {
        $condition = ucfirst($condition);

        if ($filter === null || $filter === '') {
            return false;
        }

        if (!in_array($condition, self::$allowedConditions)) {
            throw new \Exception("Condition '{$condition}' is not allowed");
        }

        $instance = self::getInstanceCondition($condition);

        $instance->setColumn($column);
        $instance->setFilter($filter);

        return $instance;
    }

}
