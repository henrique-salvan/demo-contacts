<?php

namespace App\Http\Filters;

interface FilterInterface
{
    public function apply(&$query);

    public function setColumn($column);

    public function setFilter($filter);
}
