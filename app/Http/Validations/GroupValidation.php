<?php

namespace App\Http\Validations;

class GroupValidation extends Validation {

    public function store(array $replacements = array())
    {
        return parent::store(array_replace($replacements, [
            'title' => 'required|min:2',
        ]));
    }

    public function update(array $replacements = array())
    {
        return parent::update(array_replace($replacements, [
            'title' => 'required|min:2',
        ]));
    }

}
