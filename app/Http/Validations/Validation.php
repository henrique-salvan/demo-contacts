<?php

namespace App\Http\Validations;

class Validation implements ValidationInterface
{

    public function index(array $replacements = array())
    {
        return array_replace([], $replacements);
    }

    public function show(array $replacements = array())
    {
        return array_replace([], $replacements);
    }

    public function store(array $replacements = array())
    {
        return array_replace([], $replacements);
    }

    public function update(array $replacements = array())
    {
        return array_replace([], $replacements);
    }

    public function destroy(array $replacements = array())
    {
        return array_replace([], $replacements);
    }

}
