<?php

namespace App\Http\Validations;

class UserValidation extends Validation {

    public function store(array $replacements = array())
    {
        return parent::store(array_replace($replacements, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]));
    }

    public function update(array $replacements = array())
    {
        return parent::update(array_replace($replacements, [
            'name' => 'required|min:3',
            'email' => 'required|email',
        ]));
    }

}
