<?php

namespace App\Http\Validations;

class ContactValidation extends Validation {

    public function store(array $replacements = array())
    {
        return parent::store(array_replace($replacements, [
            'name' => 'required|min:3',
            'email' => 'sometimes|email',
        ]));
    }

    public function update(array $replacements = array())
    {
        return parent::update(array_replace($replacements, [
            'name' => 'required|min:3',
            'email' => 'sometimes|email',
        ]));
    }

}
