<?php

namespace App\Http\Validations;

interface ValidationInterface
{
    public function index(array $replacements = array());

    public function show(array $replacements = array());

    public function store(array $replacements = array());

    public function update(array $replacements = array());

    public function destroy(array $replacements = array());
}
