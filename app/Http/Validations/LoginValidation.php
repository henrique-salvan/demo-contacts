<?php

namespace App\Http\Validations;

class LoginValidation extends Validation {

    public function login(array $replacements = array())
    {
        return array_replace($replacements, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
    }

}
