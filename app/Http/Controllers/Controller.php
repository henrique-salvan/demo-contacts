<?php

namespace App\Http\Controllers;

use App\Http\Traits\HttpHelpers;
use App\Http\Validations\Validation;
use App\Repositories\Repository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, HttpHelpers;

    /** @var Repository */
    protected $repository;

    /** @var Validation */
    protected $validation;

    private function scrolling()
    {
        return $this->repository->scrolling(
            $this->getFilterCollection(),
            $this->getPerPage(),
            $this->getPage(),
            $this->getWiths(),
            $this->getColumns()
        );
    }

    public function index()
    {
        $validator = validator(request()->all(), $this->validation->index());

        if ($validator->fails()) {
            return $this->transformResponse($this->getErrorsMessages($validator->errors()->toArray()), false);
        }

        return $this->transformResponse(
            $this->scrolling()
        );
    }

    public function show($id)
    {
        $validator = validator(request()->all(), $this->validation->show());

        if ($validator->fails()) {
            return $this->transformResponse($this->getErrorsMessages($validator->errors()->toArray()), false);
        }

        return $this->transformResponse(
            $this->repository->find($id, $this->getWiths(), $this->getColumns())
        );
    }

    public function store()
    {
        $validator = validator(request()->all(), $this->validation->store());

        if ($validator->fails()) {
            return $this->transformResponse($this->getErrorsMessages($validator->errors()->toArray()), false);
        }

        return $this->transformResponse(
            $this->repository->create(request()->all())
        );
    }

    public function update($id)
    {
        $validator = validator(request()->all(), $this->validation->update());

        if ($validator->fails()) {
            return $this->transformResponse($this->getErrorsMessages($validator->errors()->toArray()), false);
        }

        return $this->transformResponse(
            $this->repository->update(request()->all(), $id)
        );
    }

    public function destroy($id)
    {
        $validator = validator(request()->all(), $this->validation->destroy());

        if ($validator->fails()) {
            return $this->transformResponse($this->getErrorsMessages($validator->errors()->toArray()), false);
        }

        return $this->transformResponse(
            $this->repository->delete($id)
        );
    }

}
