<?php

namespace App\Http\Controllers;

use App\Http\Validations\ContactValidation as Validation;
use App\Repositories\ContactRepository as Repository;

class ContactsController extends Controller
{
    public function __construct(Repository $repository, Validation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }
}
