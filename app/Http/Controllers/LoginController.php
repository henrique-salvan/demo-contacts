<?php

namespace App\Http\Controllers;

use App\Http\Validations\LoginValidation;
use App\Repositories\LoginRepository;
use Hash as hash;

class LoginController extends Controller
{
    public function __construct(LoginRepository $repository, LoginValidation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }

    public function login()
    {
        $validator = validator(request()->all(), $this->validation->login());

        if ($validator->fails()) {
            return $this->transformResponse($validator->errors()->toArray(), false);
        }

        $email = request()->email;
        $password = request()->password;

        $user = $this->repository->retrievedByEmail($email);

        if (is_object($user) && hash::check($password, $user->password)) {
            return $this->transformResponse($user);
        }

        return $this->transformResponse(false);
    }

    public function user()
    {
        if (auth()->check()) {
            return $this->transformResponse(auth()->user());
        }

        return $this->transformResponse(false);
    }

}
