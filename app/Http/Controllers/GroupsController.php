<?php

namespace App\Http\Controllers;

use App\Http\Validations\GroupValidation as Validation;
use App\Repositories\GroupRepository as Repository;

class GroupsController extends Controller
{
    public function __construct(Repository $repository, Validation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }
}
