<?php

namespace App\Http\Controllers;

use App\Http\Validations\UserValidation as Validation;
use App\Repositories\UserRepository as Repository;

class UsersController extends Controller
{
    public function __construct(Repository $repository, Validation $validation)
    {
        $this->repository = $repository;
        $this->validation = $validation;
    }
}
