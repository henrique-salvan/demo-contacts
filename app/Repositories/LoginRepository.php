<?php

namespace App\Repositories;

use App\Models\User;

class LoginRepository extends Repository {

    public function __construct()
    {
        parent::__construct(User::class);
    }

    public function retrievedByEmail($email)
    {
        return $this->instanceModel->newQuery()->where('email', '=', $email)->first();
    }

}
