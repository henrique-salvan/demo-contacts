<?php

namespace App\Repositories;

use App\Http\Filters\FilterCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class Repository implements RepositoryInterface
{

    /** @var Model|Builder $instanceModel */
    public $instanceModel;

    public function __construct($abstract)
    {
        $this->instanceModel = app()->make($abstract);
    }

    public function getNewQuery()
    {
        return $this->instanceModel->newQuery();
    }

    public function scrolling(FilterCollection $filterCollection, $perPage, $page, array $with = [], array $columns = [])
    {
        return $filterCollection
                ->applies($this->getNewQuery())
                ->with($with)
                ->limit($perPage)
                ->offset(($page * $perPage) - $perPage)
                ->orderBy('id', 'DESC')
                ->get($this->columns($columns));
    }

    public function create(array $data)
    {
        $object = $this->getNewQuery()->create($data);

        if (is_object($object)) {
            return $object;
        }

        return false;
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $object = $this->instanceModel->find($id);

        if (is_object($object) && $object->update($data)) {
            return $object;
        }

        return false;
    }

    public function delete($id)
    {
        return !! $this->instanceModel->destroy($id);
    }

    public function find($id, array $with = [], array $columns = [])
    {
        return $this->getNewQuery()->with($with)->find($id, $this->columns($columns));
    }

    private function columns(array $columns = [])
    {
        return (count($columns) >= 1) ? $columns : [($this->instanceModel->getTable() . '.*')];
    }
}
