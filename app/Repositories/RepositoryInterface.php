<?php

namespace App\Repositories;

use App\Http\Filters\FilterCollection;

interface RepositoryInterface
{
    public function scrolling(FilterCollection $filterCollection, $perPage, $page, array $with = [], array $columns = []);

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, array $with = [], array $columns = ['*']);
}
