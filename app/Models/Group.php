<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = ['title'];

    protected $appends = ['to_string_computed'];

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function getToStringComputedAttribute()
    {
        return $this->title;
    }

}
