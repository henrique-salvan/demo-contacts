<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserIdScope implements Scope
{
    private $field;

    public function __construct($field = 'user_id')
    {
        $this->field = '.' . $field;
    }

    public function apply(Builder $builder, Model $model)
    {
        if (auth()->check()) {
            $builder->where($model->getTable() . $this->field, '=', auth()->user()->id);
        }

        return $builder;
    }
}
