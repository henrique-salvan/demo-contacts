<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    use SoftDeletes;

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['password'];

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }

}
