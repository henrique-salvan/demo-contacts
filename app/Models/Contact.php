<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{

    use SoftDeletes;

    protected $fillable = ['name', 'cellphone', 'email', 'group_id'];

    protected $appends = ['to_string_computed'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function getToStringComputedAttribute()
    {
        return $this->name;
    }

}
