<?php

namespace App\Models\Observers;

use App\Models\Group;

class GroupObserver
{

    public function creating(Group $group)
    {
        if (auth()->check()) {
            $group->user_id = auth()->user()->id;
        }
    }

}
