<?php

namespace App\Models\Observers;

use App\Models\Contact;

class ContactObserver
{

    public function creating(Contact $contact)
    {
        if (auth()->check()) {
            $contact->user_id = auth()->user()->id;
        }
    }

}
