<?php

namespace App\Models\Observers;

use App\Models\User;
use Hash;

class UserObserver
{

    public function creating(User $user)
    {
        $user->password = Hash::make($user->password);
        $user->api_token = md5(microtime() . uniqid());
    }

    public function updating(User $user)
    {
        if ($user->password) {
            $user->password = Hash::make($user->password);
        }
    }

}
