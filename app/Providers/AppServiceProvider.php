<?php

namespace App\Providers;

use App\Models\Contact;
use App\Models\Group;
use App\Models\Observers\ContactObserver;
use App\Models\Observers\GroupObserver;
use App\Models\Observers\UserObserver;
use App\Models\Scopes\UserIdScope;
use App\Models\User;
use Auth as auth;
use DB as db;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //maybe each of one this functions can have it own provider
        $this->bootApiDriver();
        $this->bootObservers();
        $this->bootGlobalScopes();
        $this->loggingQueries();

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function bootApiDriver()
    {
        auth::viaRequest('demo-contacts-token', function (Request $request) {

            $token = $request->header('demo-contacts-token');

            if (!$token) {
                $token = $request->{'demo-contacts-token'};
            }

            return User::query()->withoutGlobalScopes()->where('api_token', $token)->first();

        });
    }

    private function bootObservers()
    {
        User::observe(UserObserver::class);
        Contact::observe(ContactObserver::class);
        Group::observe(GroupObserver::class);
    }

    private function bootGlobalScopes()
    {
        Contact::addGlobalScope(new UserIdScope());
        Group::addGlobalScope(new UserIdScope());
        User::addGlobalScope(new UserIdScope('id'));
    }

    private function loggingQueries()
    {
        if ($this->app->environment() !== 'production') {
            db::listen(function ($query) {
                logger()->channel('queries')->debug($query->time . ': ' . vsprintf(
                    str_replace(array('%', '?'), array('%%', '%s'), $query->sql), $query->bindings
                ));
            });
        }
    }
}
