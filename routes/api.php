<?php

Route::post('login', 'LoginController@login');
Route::get('login/user', 'LoginController@user');
Route::post('users', 'UsersController@store');

Route::group(['middleware' => ['auth']], function () {
    Route::apiResource('groups', 'GroupsController');
    Route::apiResource('contacts', 'ContactsController');
    Route::apiResource('users', 'UsersController', ['only' => ['show', 'update']]);
});


