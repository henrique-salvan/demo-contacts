# Demo Contacts

An example project to share some of my skills.

## Development server

* Install a [homestead](https://laravel.com/docs/5.6/homestead) or anything you want, but make sure you have a PHP7+ and MYSQL
* Clone the project and run `composer install` inside on project folder
* Create and update your `.env` with your database configurations and after run `php artisan migrate --seed`

## Tests

* Create a database called `demo_contacts_tests`
* Run the command `phpunit` inside the project folder

## Improvements

Let me know if you have some improvement to share, please.
