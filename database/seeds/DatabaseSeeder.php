<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        factory(\App\Models\User::class, 5)->create()->each(function (\App\Models\User $user) {
            factory(\App\Models\Contact::class, 10)->create(['user_id' => $user->id]);
        });
    }
}
