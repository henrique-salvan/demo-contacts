<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Contact::class, function (Faker $faker, $attributes) {

    if (! isset($attributes['user_id'])) {
        $attributes['user_id'] = factory(App\Models\User::class)->create()->id;
    }

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'cellphone' => $faker->phoneNumber,
        'user_id' => $attributes['user_id'],
        'group_id' => function () use ($attributes) {
            return factory(App\Models\Group::class)->create(['user_id' => $attributes['user_id']])->id;
        },
    ];

});
